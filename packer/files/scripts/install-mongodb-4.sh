#!/bin/bash

sudo tee /etc/apt/sources.list.d/mongodb-org-4.0.list << EOF
deb https://repo.mongodb.org/apt/ubuntu bionic/mongodb-org/4.0 multiverse
EOF

sudo apt-key adv --keyserver hkp://keyserver.ubuntu.com --recv 9DA31620334BD75D9DCB49F368818C72E52529D4
sudo apt-get update
sudo apt-get --assume-yes install mongodb-org prometheus-mongodb-exporter
sudo systemctl start mongod
sudo systemctl enable mongod