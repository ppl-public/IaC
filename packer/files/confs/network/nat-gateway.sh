#!/bin/bash

echo "net.ipv4.ip_forward = 1" >> /etc/sysctl.conf
sysctl -p /etc/sysctl.conf
#echo 1 > /proc/sys/net/ipv4/ip_forward

echo "iptables -A POSTROUTING -t nat -s 10.10.0.0/16 -o eth0 -j MASQUERADE" >> /etc/rc.local
