#!/bin/bash

export DEBIAN_FRONTEND=noninteractive

wget -q -O - https://pkg.jenkins.io/debian/jenkins.io.key | sudo apt-key add -
sudo sh -c 'echo deb http://pkg.jenkins.io/debian-stable binary/ > /etc/apt/sources.list.d/jenkins.list'

sudo apt-get update
sudo apt-get install -yq openjdk-8-jdk-headless
sudo update-alternatives --config java
sudo update-alternatives --config javac
sudo apt-get install -yq jenkins 

#sudo systemctl enable jenkins
