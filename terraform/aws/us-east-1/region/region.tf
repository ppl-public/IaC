variable "owner"             { default = "devops" }
variable "project_name"      { default = "construtechventures" }
variable "environment"       { default = "production" }
variable "region"            { default = "us_east_1" }


resource "aws_key_pair" "default" {
  key_name   = "${var.project_name}-${var.region}-${var.environment}-${var.owner}"
  public_key = "ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABAQDJ8LdiTAgr6V+C+TmlQYx//P/3KnSqeoyI0JbcgXrcK4jleK6zRp3H6TDHBLhfTtTg4u6geAsWAvGwq/AhyWnklloD71qcipjWSdW/wcFJPmOfF3stEmMrt1f/rkRiz6X6Vf8OOu5i/ZvRfDuaxH5QD1/koKszfttukE/iG8xvnSO6McPOlMPCZKNt4dKKaDk0W7QLmvujG3PF7OWGm1sEHt0S5IVGvuCKktWibL7BpWyvsZi5NOW9wWOZv4kIThPxJho3xCw99Xf+BAjMNG2CMgGLDONaYxzoG+IBFwdX1jZ7/lB9gJu6TyphX2Tabgqw2O0tNmo8uhdEfm++T/1n caiovmv@Caios-iMac"
}