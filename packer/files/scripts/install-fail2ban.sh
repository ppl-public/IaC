#!/bin/bash

sudo apt-get update
sudo apt-get install -y fail2ban

echo "[sshd]" |sudo tee -a /etc/fail2ban/jail.local
echo "enabled = true" |sudo tee -a /etc/fail2ban/jail.local
echo "port = 22" |sudo tee -a /etc/fail2ban/jail.local
echo "filter = sshd" |sudo tee -a /etc/fail2ban/jail.local
echo "logpath = /var/log/auth.log" |sudo tee -a /etc/fail2ban/jail.local
echo "maxretry = 3" |sudo tee -a /etc/fail2ban/jail.local

sudo systemctl enable fail2ban
