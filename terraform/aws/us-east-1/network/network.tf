variable "owner"             { default = "devops" }
variable "project_name"      { default = "construtechventures" }
variable "environment"       { default = "production" }
variable "region"            { default = "us_east_1" }
variable "cidr"              { default = "10.10.0.0/16" }
variable "azs"               { default = ["us-east-1a", "us-east-1b", "us-east-1c", , "us-east-1d", "us-east-1e", "us-east-1f"] }

#resource "aws_eip" "nat" {
#  count = 3
#  vpc = true
#}

resource "aws_security_group" "default" {
  name        = "vpc-sg-default-${var.project_name}-${var.region}-${var.environment}"
  description = "Default security group for vpc-${var.project_name}-${var.region}-${var.environment}"
  vpc_id      = "${module.vpc.vpc_id}"
  ingress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["${var.cidr}"]
  }
  egress {
    from_port       = 0
    to_port         = 0
    protocol        = "-1"
    cidr_blocks     = ["10.10.0.0/16"]
  }
  tags = {
    Name        = "vpc-sg-default-${var.project_name}-${var.region}-${var.environment}"
    Owner       = "${var.owner}"
    Environment = "${var.environment}"
    Terraform   = "true"
  }
  lifecycle { create_before_destroy = true }
}

module "vpc" {
  source  = "terraform-aws-modules/vpc/aws"
  name = "vpc-${var.project_name}-${var.region}-${var.environment}"
  cidr = "10.10.0.0/16"
  tags = {
    Name        = "vpc-${var.project_name}-${var.region}-${var.environment}"
    Owner       = "${var.owner}"
    Environment = "${var.environment}"
    Terraform   = "true"
  }
  azs                 = ["us-east-1a", "us-east-1b", "us-east-1c", "us-east-1d", "us-east-1e", "us-east-1f"]
  private_subnets     = ["10.10.1.0/24", "10.10.2.0/24", "10.10.3.0/24", "10.10.4.0/24", "10.10.5.0/24", "10.10.6.0/24"]
  public_subnets      = ["10.10.11.0/24", "10.10.12.0/24", "10.10.13.0/24", "10.10.14.0/24", "10.10.15.0/24", "10.10.16.0/24"]
  database_subnets    = ["10.10.21.0/24", "10.10.22.0/24", "10.10.23.0/24", "10.10.24.0/24", "10.10.25.0/24", "10.10.26.0/24"]
  #elasticache_subnets = ["10.10.31.0/24", "10.10.32.0/24", "10.10.33.0/24", "10.10.34.0/24", "10.10.35.0/24", "10.10.36.0/24"]
  intra_subnets       = ["10.10.51.0/24", "10.10.52.0/24", "10.10.53.0/24","10.10.54.0/24", "10.10.55.0/24", "10.10.56.0/24"]

  private_subnet_tags = {
    Name        = "vpc-${var.project_name}-${var.region}-${var.environment}-private-subnet"
    Owner       = "${var.owner}"
    Environment = "${var.environment}"
    Terraform   = "true"
  }
  public_subnet_tags = {
    Name        = "vpc-${var.project_name}-${var.region}-${var.environment}-public-subnet"
    Owner       = "${var.owner}"
    Environment = "${var.environment}"
    Terraform   = "true"
  }
  database_subnet_tags = {
    Name        = "vpc-${var.project_name}-${var.region}-${var.environment}-database-subnet"
    Owner       = "${var.owner}"
    Environment = "${var.environment}"
    Terraform   = "true"
  }
  #elasticache_subnet_tags = {
  #  Name        = "vpc-${var.project_name}-${var.region}-${var.environment}-elasticache-subnet"
  #  Owner       = "${var.owner}"
  #  Environment = "${var.environment}"
  #  Terraform   = "true"
  #}
  intra_subnet_tags = {
    Name        = "vpc-${var.project_name}-${var.region}-${var.environment}-intra-subnet"
    Owner       = "${var.owner}"
    Environment = "${var.environment}"
    Terraform   = "true"
  }

  enable_dns_hostnames             = true
  enable_dns_support               = true
  enable_dhcp_options              = true
  #dhcp_options_domain_name         = "service.consul"
  #dhcp_options_domain_name_servers = ["127.0.0.1","AmazonProvidedDNS","8.8.8.8"]
  #dhcp_options_ntp_servers        = ["127.0.0.1"]

  #### WARNING ####
  # If you are crazy and want to pay more than a t3.medium instance per NAT Gateway change enable_nat_gateway to true
  enable_nat_gateway = false
  single_nat_gateway = true
  one_nat_gateway_per_az = false
  #reuse_nat_ips       = true                      # <= Skip creation of EIPs for the NAT Gateways
  #external_nat_ip_ids = ["${aws_eip.nat.*.id}"]   # <= IPs specified here as input to the module
  enable_vpn_gateway = false
  
  # Take a good look Endpoints works and how it is billed before enable them
  # VPC endpoint for S3
  #enable_s3_endpoint = true
  # VPC endpoint for DynamoDB
  #enable_dynamodb_endpoint = true
  # VPC endpoint for SSM
  #enable_ssm_endpoint              = true
  #ssm_endpoint_private_dns_enabled = true
  #ssm_endpoint_security_group_ids  = ["${aws_security_group.default.id}"] # ssm_endpoint_subnet_ids = ["..."]
  # VPC endpoint for SSMMESSAGES
  #enable_ssmmessages_endpoint              = true
  #ssmmessages_endpoint_private_dns_enabled = true
  #ssmmessages_endpoint_security_group_ids  = ["${aws_security_group.default.id}"]
  # VPC Endpoint for EC2
  #enable_ec2_endpoint              = true
  #ec2_endpoint_private_dns_enabled = true
  #ec2_endpoint_security_group_ids  = ["${aws_security_group.default.id}"]
  # VPC Endpoint for EC2MESSAGES
  #enable_ec2messages_endpoint              = true
  #ec2messages_endpoint_private_dns_enabled = true
  #ec2messages_endpoint_security_group_ids  = ["${aws_security_group.default.id}"]
  # VPC Endpoint for ECR API
  #enable_ecr_api_endpoint              = true
  #ecr_api_endpoint_private_dns_enabled = true
  #ecr_api_endpoint_security_group_ids  = ["${aws_security_group.default.id}"]
  # VPC Endpoint for ECR DKR
  #enable_ecr_dkr_endpoint              = true
  #ecr_dkr_endpoint_private_dns_enabled = true
  #ecr_dkr_endpoint_security_group_ids  = ["${aws_security_group.default.id}"]

  # Enable Public Access to RDS instances
  #create_database_subnet_group           = true
  #create_database_subnet_route_table     = true
  #create_database_internet_gateway_route = true

}
