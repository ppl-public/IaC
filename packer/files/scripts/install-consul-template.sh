#!/bin/bash

sudo apt-get install -y gunzip

echo "################################"
echo " Consul Template Verion $1 "
echo "################################"

sudo wget https://releases.hashicorp.com/consul-template/${CONSUL_TEMPLATE_VERSION}/consul-template_${CONSUL_TEMPLATE_VERSION}_linux_amd64.tgz -P /tmp
sudo file consul-template_${CONSUL_TEMPLATE_VERSION}_linux_amd64.tgz
cd /tmp
sudo tar -xvzf consul-template_${CONSUL_TEMPLATE_VERSION}_linux_amd64.tgz
sudo mv consul-template /usr/sbin