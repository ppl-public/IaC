#!/bin/bash

export DEBIAN_FRONTEND=noninteractive

sudo apt-get update
sudo apt-get install -yyq build-essential golang-docker-credential-helpers python python-pip python3 python3-pip