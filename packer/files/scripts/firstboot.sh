#!/bin/bash

    export DEBIAN_FRONTEND=noninteractive
    
    function verify_memory {
        # Verifies how much swap memory needs to be allocated
        if [ $(sudo free -m| grep  Mem | awk '{ print int($2) }') -lt "1024" ]
         then
            SWAP=2G
            SWAPPINESS=60
            CACHE_PRESSURE=600
            MIN_FREE_KBYTES=62915

        elif [ $(sudo free -m| grep  Mem | awk '{ print int($2) }') -gt "1024" ] && [ $(sudo free -m| grep  Mem | awk '{ print int($2) }') -lt "2048" ]
         then
            SWAP=3G
            SWAPPINESS=50
            CACHE_PRESSURE=500
            MIN_FREE_KBYTES=125830

        else
            SWAP=4G
            SWAPPINESS=40
            CACHE_PRESSURE=400
            MIN_FREE_KBYTES=251658
        fi
    }
    function config_swap {
        verify_memory
        sudo fallocate -l ${SWAP} /swapfile
        sudo chmod 600 /swapfile
        sudo mkswap /swapfile
        sudo swapon /swapfile
        echo '/swapfile none swap sw 0 0' | tee -a /etc/fstab
        echo 'vm.swappiness='${SWAPPINESS}'' | tee -a /etc/sysctl.conf
        echo 'vm.vfs_cache_pressure='${CACHE_PRESSURE}'' | tee -a /etc/sysctl.conf
        echo 'vm.min_free_kbytes='${MIN_FREE_KBYTES}'' | tee -a /etc/sysctl.conf

        sudo sysctl -p
    }
    function config_apt {
        echo 'APT::Install-Recommends "0";' |sudo tee -a /etc/apt/apt.conf.d/01norecommend
        echo 'APT::Install-Suggests "0";' |sudo tee -a /etc/apt/apt.conf.d/01norecommend
        echo 'APT::Options "--force-confold";' |sudo tee -a /etc/apt/apt.conf.d/01norecommend
    }
    function update_os {
        export DEBIAN_FRONTEND=noninteractive
        sudo apt-get update && sudo apt-get upgrade -yqq && sudo apt-get dist-upgrade -yqq
    }
    function install_basic_pkgs {
        export DEBIAN_FRONTEND=noninteractive
        sudo apt-get update && sudo apt-get install --no-install-recommends -yqq dirmngr software-properties-common apt-transport-https ca-certificates ca-certificates gnupg2 chrony
    }
    function install_basic_tools {
        export DEBIAN_FRONTEND=noninteractive
        sudo apt-get update && sudo apt-get install --no-install-recommends -yqq awscli zip unzip jq
    }
    function install_net_tools {
        export DEBIAN_FRONTEND=noninteractive
        sudo apt-get update && sudo apt-get install --no-install-recommends -yqq git curl wget ftp telnet netcat
    }
    function install_monitoring_tools {
        export DEBIAN_FRONTEND=noninteractive
        sudo echo y| add-apt-repository ppa:prometheus-release-tracking/prometheus-exporters
        sudo apt-get update && sudo apt-get install --no-install-recommends -yqq sysstat nmon htop iotop iptraf-ng lsof netdata prometheus-node-exporter
    }
    function config_locale {
        LOCALE="en_US.UTF-8"
        #sudo apt-get install --no-install-recommends -y language-pack-en
        sudo locale-gen $LOCALE
    }
    function config_timezone {
        TIMEZONE="UTC"
        sudo ln -sf /usr/share/zoneinfo/$TIMEZONE /etc/localtime
    }
    function copy_myself {
        sudo cp /tmp/files/scripts/firstboot.sh /root/firstboot.sh
        sudo chmod + /root/firstboot.sh
        printf '%s\n' '#!/bin/bash' '/bin/touch /root/firstboot.key' '/root/firstboot.sh' 'exit 0' | sudo tee -a /etc/rc.local
        sudo chmod +x /etc/rc.local
    }
    function uninstall_snap {
        sudo apt-get remove -y snapd snap
        sudo apt-get purge -y snapd snap
    }
    function uninstall_lxd {
        sudo apt-get remove -y lxd lxc
        sudo apt-get purge -y lxd lxc
    }
    function disable_unwanted_services {
        sudo systemctl disable apt-daily-upgrade.timer
        sudo systemctl disable apt-daily.timer
        sudo systemctl disable motd-news.timer
        sudo systemctl disable lxd.socket
        sudo systemctl disable lxd-containers.service 
        sudo systemctl disable lxcfs.service
        sudo systemctl disable ufw.service
        sudo systemctl disable atd.service
        sudo systemctl disable iscsid.socket
    }
    function clean-apt {
        sudo apt-get autoremove -y
        sudo apt-get clean -y
        sudo sync
    }
    function install {
        config_apt
        update_os
        install_basic_pkgs
        install_basic_tools
        install_net_tools
        install_monitoring_tools
        config_locale
        config_timezone
        copy_myself
        uninstall_snap
        uninstall_lxd
        disable_unwanted_services
        clean-apt
    }
    function firstboot {
        verify_memory
        config_swap
    }
    function osupdate {
        update_os
        autoremove
    }

    if [ ! -f /root/install.key ]
    then
        install
        sudo touch /root/install.key
    else
        echo "install script already been executed" 
    fi

    if [ ! -f /root/firstboot.key ]
    then
        exit 0
    else
        echo "config swap"
        firstboot
        sudo rm /root/firstboot.key
        sudo sed -i '/firstboot.key/d' /etc/rc.local

    fi

    if [ ! -f /root/osupdate.key ]
    then
        exit 0
    else
        echo "update main components"
        osupdate
        sudo rm /root/osupdate.key
    fi
