#!/bin/bash

echo "deb https://packages.grafana.com/oss/deb stable main" |sudo tee /etc/apt/sources.list.d/grafana.list
curl https://packages.grafana.com/gpg.key | sudo apt-key add -
sudo apt-get update
sudo apt-get install -yqq libfontconfig grafana prometheus prometheus-node-exporter

