variable "owner"             { default = "devops" }
variable "project_name"      { default = "construtechventures" }
variable "environment"       { default = "production" }
variable "region"            { default = "us_east_1" }
variable "cidr"              { default = "10.10.0.0/16" }
variable "azs"               { default = ["us-east-1a", "us-east-1b", "us-east-1c", , "us-east-1d", "us-east-1e", "us-east-1f"] }

module "region" {
  source = "../../../aws/us-east-1/region/"
    owner = "${var.owner}"
}

module "network" {
  source = "../../../aws/us-east-1/network/"
  owner = "${var.owner}"
  project_name = "${var.project_name}"
  environment = "${var.environment}"
  region = "${var.region}"
  cidr = "${var.cidr}"
  azs = "${var.azs}"
}
