#!/bin/bash

sudo apt-get update
sudo apt-get install -y apt-transport-https ca-certificates curl software-properties-common

sudo add-apt-repository ppa:vbernat/haproxy-1.9
sudo apt-get update

sudo apt-get install -y haproxy=1.9.\*

sudo apt-get update && sudo apt-get install --no-install-recommends -yqq prometheus-haproxy-exporter

